<?php
/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   computorv1.php                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: aiholkin <aiholkin@student.42.fr>          +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2018/09/02 16:16:13 by aiholkin          #+#    #+#             */
/*   Updated: 2018/09/02 16:18:20 by aiholkin         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

//# Examples:
//#  5 * X^0 + 4 * X^1 - 9.3 * X^2 = 1 * X^0
//#  4 * X^0 + 4 * X^1 - 9.3 * X^2 = 0
//#  5 * X^0 + 4 * X^1 = 4 * X^0
//#  1 * X^0 + 4 * X^1 = 0
//#  8 * X^0 - 6 * X^1 + 0 * X^2 - 5.6 * X^3 = 3 * X^0
//#  5 * X^0 - 6 * X^1 + 0 * X^2 - 5.6 * X^3 = 0
//#  5 + 4 * X + X^2= X^2
//   42 ∗ X^0 = 42 ∗ X^0
//   x^2 + 2*x + 17 = 0

//eq_part structure
/*
 * [
 * sign: +/-
 * pow:  (-1), 0, 1, 2          (-1) no power,
 * k: coefficient
 * var: is variable or just number
 * ]
 * */

function parse($eq, &$eq_parts, &$max_degree){
    $eq = preg_replace('/\s+/', '', $eq);
    $eq = strtolower(explode('=', $eq)[0]);
    if(preg_match('/\^-[0-9]{0,}/', $eq)){
        echo 'Can\'t solve equasiton with negative degree';
        exit(1);
    }
    $parts = preg_split('/([-+]{1,1})/', $eq, -1, PREG_SPLIT_DELIM_CAPTURE | PREG_SPLIT_NO_EMPTY);
    $last_sign = '+';
    foreach ($parts as $i => $part_data){
        if($part_data === '-' || $part_data === '+'){
            $last_sign = $part_data;
            continue ;
        }
        $tmp = [];
        if(preg_match('/(^[0-9]+$)|(^[0-9].[0-9]+$)/', $part_data)){
            $tmp = [
                'pow'   => -1,
                'k'     => $last_sign === '-' ? floatval($part_data) * -1 : floatval($part_data),
                'var'   => false,
            ];
        }else if (preg_match('/[0-9.]*[\*x](\^[0-9]*)*/', $part_data)) {
            $tmp = [
                'pow'   => 1,
                'k'     => $last_sign === '-' ? -1 : 1,
                'var'   => true,
            ];
            $k_v = explode('*', $part_data);
            $v_pow = explode('^', $part_data);
            if(count($k_v) > 1){
                $tmp['k'] = $last_sign === '-' ? floatval($k_v[0]) * -1 : floatval($k_v[0]);
            }
            if(count($v_pow) > 1){
                $tmp['pow'] = intval($v_pow[1]);
                if($tmp['pow'] > $max_degree)
                    $max_degree = $tmp['pow'];
                if($tmp['pow'] > 2){
                    echo "The polynomial degree is stricly greater than 2, I can't solve.\n";
                    exit(1);
                }
            }
        }
        $eq_parts[] = $tmp;
    }
}

function reduce($eq){
    $part_arr = explode('=', $eq);
    if(!isset($part_arr[1])){
        echo 'Syntx error after "="'.PHP_EOL;
        exit(1);
    }
    $right_part = $part_arr[1];
    $right_part_cleaned = preg_replace('/\s+/', '', $right_part);
    if(strlen($right_part_cleaned) === 1 && preg_match('/0{1,1}/', strval($right_part_cleaned)) && intval($right_part_cleaned) === 0){
        return $eq;
    }else{
        $j = 0;
        $len = strlen($right_part);
        while ($j < $len){
            if($right_part[$j] === '-'){
                $right_part = substr_replace($right_part, '+', $j, 1);
            }else if($right_part[$j] === '+'){
                $right_part = substr_replace($right_part, '-', $j, 1);
            }
            $j++;
        }

        if($right_part_cleaned[0] !== '-'){
            $right_part = ' -'.$right_part;
        }
    }
    return preg_replace('/\s{2,}/', ' ', $part_arr[0].$right_part.' = 0');
}

function compute($eq_parts){
    $counted_parts = [];

    foreach ($eq_parts as $poly_part) {
        // if($poly_part['pow'] === -1 || $poly_part['pow'] === 0){
            if($poly_part['pow'] === 0)
                $poly_part['pow'] = -1;
            if(isset($counted_parts[strval($poly_part['pow'])])){
                $existing_k_sign = $counted_parts[strval($poly_part['pow'])]['sign'];
                $curr_k_sign = $poly_part['sign'];
                $existing_k = $counted_parts[strval($poly_part['pow'])]['k'];
                $curr_k = $poly_part['k'];



                $counted_parts[strval($poly_part['pow'])]['k'] =  $counted_parts[strval($poly_part['pow'])]['k'] + $poly_part['k'];
            }else{
                $counted_parts[strval($poly_part['pow'])] = $poly_part;
            }
        // }
    }
    return $counted_parts;
}

function solve($parts){
        $a = isset($parts['2']) ? $parts['2']['k'] : 0;
        $b = isset($parts['1']) ? $parts['1']['k'] : 0;
        $c = isset($parts['-1']) ? $parts['-1']['k'] : 0;

        if(($b > 0.0000001 || $b < -0.0000001) && ($a > 0.0000001 || $a < -0.0000001)){
            $d = ($b * $b) - (4 *  $a * $c);

            echo "Find dicriminant: D = $b^2 - 4 * $a * $c = $d\n";

            if($d < 0.0000001 && $d > -0.0000001){
                $sol = -($b / (2 * $a));
                echo "Discriminant equals 0, one solution is:\n".$sol.PHP_EOL;
            }else if($d > 0){
                $sol_1 = (-$b + sqrt($d)) / (2 * $a);
                $sol_2 = (-$b - sqrt($d)) / (2 * $a);
                echo "Discriminant is strictly positive, the two solutions are:\n".$sol_1."\n".$sol_2."\n";
            }else{
                echo "Discriminent is strictly negative, the two solutions are:\n";
                echo (-$b / (2*$a))." + ".myabs((sqrt(myabs($d)) / (2*$a)))."i".PHP_EOL;
                echo (-$b / (2*$a))." - ".myabs((sqrt(myabs($d)) / (2*$a)))."i".PHP_EOL;
            }
        }else if (($b > 0.0000001 || $b < -0.0000001) && ($a < 0.0000001 && $a > -0.0000001)) {
            $sol = -$c / $b;
            echo "Solving simple equasion of degree 1, solution is:\n".$sol."\n";
        }else if(($b < 0.0000001 && $b > -0.0000001) && ($a > 0.0000001 || $a < -0.0000001)){
            $before_sqrt = ($c * -1) / $a;
            if($before_sqrt > 0){
                $sol = sqrt($before_sqrt);
            }else{
                $sol = '+/-(i'.sqrt(myabs($c / $a)).')';
            }
            echo "Solving simple equasion of degree 2, solution is:\n".$sol."\n";
        }else{
            if(($a < 0.0000001 && $a > -0.0000001) && ($b < 0.0000001 && $b > -0.0000001) && ($c < 0.0000001 && $c > -0.0000001))
                echo "Every real is a solution\n";
            else
                echo "Wrong equation, no solution";
        }

}

function myabs($num){
    if($num < 0)
        return $num * -1;
    return $num;
}

$max_degree = 1;
$eq_parts = [];

if(!isset($argv[1]) || strlen($argv[1]) === 0){
    echo "Nothing to be done\n";
}else{
    $eq = reduce($argv[1]);
    // echo "Reduced form: ".$eq.PHP_EOL;
    parse($eq, $eq_parts, $max_degree);
    echo "Polynomial degree: ".$max_degree.PHP_EOL;
    $counted_parts = compute($eq_parts);
    $reduced_form = '';
    foreach ($counted_parts as $key => $value) {
        if($key == '-1'){
            $tmp = strval(($value['k'] >= 0 ? ' + '.strval($value['k']) : ' - '.strval($value['k']) * (-1)));
            $reduced_form .= ' '.$tmp.' ';
        }else if($value['k'] > 0.0000001 || $value['k'] < -0.0000001){
            if($key == '1'){
                $tmp = strval(($value['k'] >= 0 ? ' + '.strval($value['k']) : ' - '.strval($value['k']) * (-1)));
                $reduced_form .= ' '.$tmp.'x ';
            }else if($key == '2'){
                $tmp = strval(($value['k'] >= 0 ? ' + '.strval($value['k']) : ' - '.strval($value['k']) * (-1)));
                $reduced_form .= ' '.$tmp.'x^2 ';
            }
        }
    }
    $reduced_form = trim($reduced_form);
    $reduced_form = preg_replace('/\s{2,}/', ' ', $reduced_form);
    $reduced_form = preg_replace('/^\+{1,1}/', '', $reduced_form);
    echo "Reduced form: ".$reduced_form.' = 0'.PHP_EOL;

    solve($counted_parts);
}
?>